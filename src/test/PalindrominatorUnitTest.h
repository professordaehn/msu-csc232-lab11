/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    PalindrominatorUnitTest.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Palindrominator CPPUNIT test specification. DO NOT MODIFY THE CONTENTS OF THIS FILE!
 * @see     http://sourceforge.net/projects/cppunit/files
 */

#ifndef LAB11_PALINDROMINATORUNITTEST_H
#define LAB11_PALINDROMINATORUNITTEST_H

#include <cppunit/extensions/HelperMacros.h>

class PalindrominatorUnitTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(PalindrominatorUnitTest);
        CPPUNIT_TEST(testOddLengthPalindrome);
        CPPUNIT_TEST(testEvenLengthPalindrome);
        CPPUNIT_TEST(testOddLengthNonPalindrome);
        CPPUNIT_TEST(testEvenLengthNonPalindrome);
    CPPUNIT_TEST_SUITE_END();

public:
    PalindrominatorUnitTest();
    virtual ~PalindrominatorUnitTest() {}
    void setUp();
    void tearDown();
    void testOddLengthPalindrome();
    void testEvenLengthPalindrome();
    void testOddLengthNonPalindrome();
    void testEvenLengthNonPalindrome();
};

#endif // LAB11_PALINDROMINATORUNITTEST_H
