/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Palindrominator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Palindrominator implementation.
 */

#include "Palindrominator.h"

bool Palindrominator::isPalindrome(const std::string text) {
    // TODO: Implement me using the encapsulated queue and stack
    return false;
}